<?php

return function( $site, $pages, $page ) {

	$query   = get( 'q' );
	$results = page( 'blog' )->search( $query, 'title|text' );
	$results = $results->paginate(20);

	return array(
		'query'      => $query,
		'results'    => $results,
		'pagination' => $results->pagination()
	);

};