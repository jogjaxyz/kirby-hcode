<?php 

return function( $site, $pages, $page ) {

  // fetch the basic set of pages
  $related = $page->children()->visible()->flip();

  // add the tag filter
  if ( $cats = param( 'category' ) ) {
    $related = $related->filterBy( 'category', $cats, ',' );
  }

  // fetch all tags
  $cats = $related->pluck( 'category', ',', false );

  // apply pagination
  $related    = $related->paginate(10);
  $pagination = $related->pagination();

  return compact( 'related', 'cats', 'category', 'pagination' );

};