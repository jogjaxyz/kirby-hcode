<div class="widget widget_tag_cloud" id="tag_cloud-9">
	<h5 class="widget-title font-alt"><?php echo l::get( 'Tags Cloud' ); ?></h5>
	<div class="thin-separator-line bg-dark-gray no-margin-lr"></div><!-- end heading -->
	<div class="tagcloud">
		<div class="tags_cloud tags">
			<?php
				$tags = $pages->find( 'blog' )->children()->visible()->pluck( 'tags', ',', true );
			?>
			<?php foreach( $tags as $tag ) : ?>
				<a class="tag-link-12" href="<?php echo url( 'blog/tag:' . $tag );?>"><?php echo html( $tag ); ?></a>
			<?php endforeach; ?>
		</div>
	</div>
</div>