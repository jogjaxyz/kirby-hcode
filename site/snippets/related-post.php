		 
		<?php 
			$options 		= array( 'searchField' => 'tags' ); 
			$relatedPosts 	= relatedpages( $options );
		?>
		<?php if ( strlen( $relatedPosts ) != 0 && isset( $relatedPosts ) ) : ?>

		<!-- SECTION RELATED POST -->
		<section class="no-padding clear-both">
			<div class="container">
				<div class="row">
					<div class="col-md-12 no-padding">
						<div class="hcode-divider border-top xs-no-padding-bottom xs-padding-five-top margin-five-top padding-five-bottom"></div>
					</div>
					<div class="col-md-12 col-sm-12 center-col text-center margin-eight no-margin-top xs-padding-ten-top">
						<h3 class="blog-single-full-width-h3"><?php echo l::get( 'Related Blogs' ); ?></h3>
					</div>
					<div class="blog-grid-listing padding-ten-bottom col-md-12 col-sm-12 col-xs-12 no-padding">
						
						<?php foreach ( $relatedPosts as $item ) : ?>
							
							<div class="col-md-4 col-sm-4 col-xs-12 blog-listing no-margin-bottom xs-margin-bottom-ten wow fadeInUp animated" data-wow-duration="300ms">
								<div class="blog-image">
									<a href="<?php echo $item->url(); ?>" itemprop="url">
										<?php $image = thumb( $item->image(), array( 'width' => 760, 'height' => 570, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?><img src="<?php echo $image->url(); ?>" width="760" height="570">
									</a>
								</div>
								<div class="blog-details no-padding">
									<div class="blog-date" itemprop="datePublished">
										Posted by <?php echo $item->author(); ?> | <?php echo $item->date( 'd F Y' ); ?>  | <?php getPostCategories( $item ); ?>
									</div>
									<h3 class="blog-title" itemprop="name">
										<a href="<?php echo $item->url(); ?>" itemprop="url"><?php echo $item->title(); ?></a>
									</h3>
									<div class="blog-short-description">
										<?php echo getPostExcerpt( $item, 50 ); ?>
									</div>
								</div>
							</div>

						<?php endforeach; ?>

					</div>
				</div>
			</div>
		</section>
		
		<?php endif; ?>
