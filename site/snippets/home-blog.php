		
			<!-- SECTION LATEST BLOG -->
			<section class="section-blog">
				<div class="container">
					<div class="row">
						<div class="col-xs-mobile-fullwidth text-center">
							<div class="column-innner-wrapper">
								<h2 class="section-title black-text"><?php echo l::get( 'Latest Blogs' ); ?></h2>
							</div>
						</div>
						<?php
							$articles = $pages->find( 'blog' )
										->children()
										->visible()
										->flip()
										->limit(3); 
							$counter 	= 300;
						 ?>
						 <?php foreach ( $articles as $post ) : ?>
						
							<div class="col-md-4 col-xs-mobile-fullwidth col-sm-4 xs-margin-three-bottom wow fadeInUp" data-wow-duration="<?php echo $counter; ?>ms">
								<div class="column-innner-wrapper">
									<div class="blog-post">
										<div class="blog-image">
											<?php if ( $post->image() ) : ?>
												<a href="<?php echo $post->url(); ?>" itemprop="url">
													<?php $featured_image =  thumb( $post->image(), array( 'width' => 760, 'height' => 570, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
													<img alt="760x570-ph alt" class="attachment-full size-full wp-post-image" height="570" src="<?php echo $featured_image->url(); ?>" title="" width="760">
												</a>
											<?php endif; ?>
										</div>
										<div class="post-details">
											<h3 itemprop="name"><a class="post-title text-med sm-margin-top-ten xs-no-margin-top" href="<?php echo $post->url(); ?>" itemprop="url">
												<?php echo html::decode( $post->title()->kirbytext() ); ?>
											</a></h3>
											<i class="post-author light-gray-text2" itemprop="datePublished">
												Posted by <?php echo $post->author(); ?> | <?php echo $post->date( 'd F Y' );  ?>
											</i>
											<p><?php echo excerpt( $post->text(), 200 ); ?></p>
										</div>
									</div>
								</div>
							</div>
						 <?php  $counter += 300; endforeach; ?>
						
						<div class="col-xs-mobile-fullwidth">
							<div class="column-innner-wrapper">
								<div class="hcode-space margin-four-bottom"></div>
							</div>
						</div>
						<div class="col-md-12 col-xs-mobile-fullwidth col-sm-12 text-center wow fadeInUp" data-wow-duration="1200ms">
							<div class="column-innner-wrapper">
								<a class="inner-link highlight-button-dark btn-small button btn" href="<?php echo page( 'blog' )->url(); ?>" target="_self"><?php echo l::get( 'View All Blog' ); ?></a>
							</div>
						</div>
					</div>
				</div>
			</section>
			
