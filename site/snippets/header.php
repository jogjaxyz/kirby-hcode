<!doctype html>
<html lang="<?php echo site()->language() ? site()->language()->code() : 'en' ?>">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
	<?php if ( $page->description()->isNotEmpty() ) : ?>
	<meta name="description" content="<?php echo $page->description()->excerpt(160) ?>">
	<?php endif; ?>
	<?php if ( $site->favicon()->isNotEmpty() ) : ?>
	<link rel="shortcut icon" href="<?php echo $site->favicon()->toFile()->url(); ?>">
	<?php endif; ?>
	<?php if ( $site->appleTouch57()->isNotEmpty() ) : ?>
	<link rel="apple-touch-icon" href="<?php echo $site->appleTouch57()->toFile()->url(); ?>">
	<?php endif; ?>
	<?php if ( $site->appleTouch72()->isNotEmpty() ) : ?>
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $site->appleTouch72()->toFile()->url(); ?>">
	<?php endif; ?>
	<?php if ( $site->appleTouch114()->isNotEmpty() ) : ?>
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $site->appleTouch114()->toFile()->url(); ?>">
	<?php endif; ?>
	<?php if ( $site->appleTouch149()->isNotEmpty() ) : ?>
	<link rel="apple-touch-icon" sizes="149x149" href="<?php echo $site->appleTouch149()->toFile()->url(); ?>">
	<?php endif; ?>

	<?php echo css( 'assets/css/plugins.min.css' ); ?>

	<?php echo css( 'assets/css/style.css' ); ?>
	
	<?php echo js( 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' ); ?>

</head>
<body>
	<!-- HEADER -->
	<nav class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-dark-transparent nav-white">
		<div class="container">
			<div class="row">
				<div class="col-md-2 pull-left">
					<?php if ( ! $site->headerLogoLight()->isEmpty() ) : ?>

						<a class="logo-light" href="<?php echo $site->url(); ?>">
							<img alt="Header Logo" class="logo" src="<?php echo $site->headerLogoLight()->toFile()->url(); ?>"> 
						</a>
					<?php endif; ?> 
					<?php if ( ! $site->headerLogoDark()->isEmpty() ) : ?>
						
						<a class="logo-dark" href="<?php echo $site->url(); ?>">
							<img alt="Header Logo" class="logo" src="<?php echo $site->headerLogoDark()->toFile()->url(); ?>"> 
						</a>
					 <?php endif; ?> 
				</div>

				<div class="col-md-2 no-padding-left search-cart-header pull-right">
					<div id="top-search">
						<a class="header-search-form" href="#search-header"><i class="fa fa-search search-button"></i></a>
					</div>
					<?php snippet( 'search-form' ); ?>
					<?php snippet( 'language-switcher' ); ?>
				</div>

				<div class="navbar-header col-sm-8 col-xs-2 pull-right">
					<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
				</div>

				<?php snippet( 'menu' ); ?>
			</div>
		</div>
	</nav>
	