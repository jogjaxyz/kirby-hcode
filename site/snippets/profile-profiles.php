				
				<!-- SECTION PROFILES -->
				<section class="section-profiles">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<h2 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->profileTitle(); ?></h2>
									<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper content-left">
									<?php echo $page->profileLeftContent()->kirbytext(); ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper content-right">
									<?php echo $page->profileRightContent()->kirbytext(); ?>
								</div>
							</div>
						</div>
					</div>
				</section>