			
			<!--  SECTION HOME FEATURES -->
			<section class="section-featured-services parallax-fix parallax2 no-padding" data-bg-image="<?php if ( $page->featuredServiceImage() ) echo $page->featuredServiceImage()->toFile()->url(); ?>">
				<div class="col-md-6 col-xs-mobile-fullwidth col-sm-6 no-padding">
					<div class="column-innner-wrapper"></div>
				</div>
				<div class="col-md-6 col-xs-12 no-padding" data-bg-color="rgba(0,0,0,0.8)">
					<div class="column-innner-wrapper">
						<div class="panel-group accordion-style4 about-tab-right" id="1445319542">
						<?php 
							$counter = 1;
							foreach ( $page->featuredServicesitems()->toStructure() as $item ) : ?>
								
								<div class="panel panel-default">
									<div class="panel-heading <?php echo ( 1 == $counter ) ? 'active-accordion' : ''; ?>">
										<a data-parent="#1445319542" data-toggle="collapse" href="#accordian-panel-<?php echo $counter; ?>">
											<h2 class="panel-title">
												<i class="icon-heart extra-small-icon vertical-align-middle"></i> 
												<?php echo html::decode( $item->title()->kirbytext() ); ?>
												<span class="pull-right">
													<?php echo ( 1 == $counter ) ? '<i class="fa fa-minus"></i>' : '<i class="fa fa-plus"></i>'; ?>
												</span>
											</h2>
										</a>
									</div>
									<div class="panel-collapse collapse <?php echo ( 1 == $counter ) ? 'in' : ''; ?>" id="accordian-panel-<?php echo $counter; ?>">
										<div class="panel-body">
											<div class="col-md-2 col-sm-2 col-xs-6 no-padding xs-margin-bottom-five">
												<?php if ( $item->image() ) : ?>
													<?php $image =  thumb( $page->image( $item->image() ), array( 'width' => 500, 'height' => 500, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
													<img alt="500x500 alt" class="white-round-border no-border spa-packages-img" width="500" height="500" src="<?php echo $image->url(); ?>" >
												<?php endif; ?>
											</div>
											<div class="col-md-9 col-sm-9 col-xs-12 sm-pull-right col-md-offset-1 no-padding">
												<p class="text-med light-gray-text">
													<?php echo html::decode( $item->content()->kirbytext() ); ?>
												</p>
												<a class="highlight-button-pink" href="<?php echo html::decode( $item->link()->kirbytext() ); ?>"><?php echo l::get( 'Read More' ); ?></a>
											</div>
										</div>
									</div>
								</div>
							<?php $counter++; endforeach; ?>
						</div>
					</div>
				</div>
			</section>
			
