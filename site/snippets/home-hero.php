			
			<!-- SECTION HERO -->
			<section class="full-width-image owl-half-slider parallax-fix parallax2 no-padding" data-bg-image="<?php echo $page->heroBgImage()->toFile()->url(); ?>">
				<div class="selection-overlay" data-bg-color="#000000" data-opacity="0.5"></div>
				<div class="container">
					<div class="row">
						<div class="col-xs-mobile-fullwidth">
							<div class="column-innner-wrapper">
								<div class="container position-relative">
									<div class="slider-typography text-center padding-thirteen-top">
										<div class="slider-text-middle-main">
											<div class="slider-text-middle padding-left-right-px animated fadeInUp">
												<h2 class="owl-subtitle" data-color="#ffffff">
													<?php echo html::decode( $page->heroTitle()->kirbytext() ); ?>
												</h2>
												<div class="separator-line-thick margin-three" data-bg-color="#e6af2a"></div>
												<h3 class="owl-title center-col margin-ten no-margin-top white-text">
													<?php echo html::decode( $page->heroSubtitle()->kirbytext() ); ?>
												</h3>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>