<div class="blog-listing blog-listing-classic">
	<div class="blog-image">
		<a href="<?php echo getPostUrl( $item ); ?>" itemprop="url">
			<?php $image = thumb( $item->image(), array( 'width' => 760, 'height' => 570, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?><img src="<?php echo $image->url(); ?>" width="760" height="570">
		</a>
	</div>
	<div class="blog-details">
		<div class="blog-date">
			<?php echo l::get( 'Posted by' ); ?> <?php echo $item->author(); ?> | <?php echo $item->date( 'd F Y' ); ?> | <?php getPostCategories( $item ); ?>
		</div>
		<h2 class="blog-title" itemprop="name">
			<a href="<?php echo getPostUrl( $item ); ?>" itemprop="url"><?php echo $item->title(); ?></a>
		</h2>
		<div class="margin-four-bottom">
			<?php echo getPostExcerpt( $item, 100 ); ?>
		</div>
		<a class="highlight-button btn btn-small xs-no-margin-bottom" href="<?php echo getPostUrl( $item ); ?>" itemprop="url"><?php echo l::get( 'Continue Reading' ); ?></a>
	</div>
</div>

	