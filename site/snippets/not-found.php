<div class="col-md-8 col-sm-8 col-xs-12">
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
		<div class="alert alert-warning fade in" role="alert">
			<i class="fa fa-question-circle alert-warning"></i> <strong><?php echo l::get( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.' ); ?></strong>
		</div>
	</div>
</div>