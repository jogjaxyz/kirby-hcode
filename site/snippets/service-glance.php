				
				<!-- SECTION GLANCE -->
				<section class="section-glance" data-bg-color="#000000">
					<div class="container">
						<div class="row">
							<div class="column-innner-wrapper gray-text">
								<div class="col-sm-12 col-xs-mobile-fullwidth">
									<h3 class="title-large text-uppercase letter-spacing-1 font-weight-600 white-text"><?php echo $page->glanceTopTitle(); ?></h3>
									<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
								</div>
							</div>
							<div class="col-md-6 col-xs-mobile-fullwidth col-sm-12 xs-padding-top-six sm-padding-top-three ipad-padding-top">
								<div class="column-innner-wrapper">
									<?php if ( $page->glanceTopImage() != '' ) : ?>
										<?php $image =  thumb( $page->image( $page->glanceTopImage() ), array( 'width' => 800, 'height' => 600, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
										
										<img alt="" class="xs-img-full" height="800" width="800" src="<?php echo $image->url(); ?>">
									<?php endif; ?>

								</div>
							</div>
							<div class="col-md-6 col-xs-mobile-fullwidth col-sm-12 xs-padding-top-six sm-padding-top-three ipad-padding-top">
								<div class="column-innner-wrapper gray-text">
									<?php echo $page->glancetTopContent()->kirbytext(); ?>

								</div>
							</div>
						</div>

						<div class="row padding-six-top">
							<div class="col-sm-12 col-xs-mobile-fullwidth ">
								<h3 class="title-large text-uppercase letter-spacing-1 font-weight-600 white-text"><?php echo $page->glanceBottomTitle(); ?></h3>
								<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
							</div>
							<?php if ( s::get( 'device_class' ) == 'mobile' ) : ?>
							
								<div class="col-md-6 col-xs-mobile-fullwidth col-sm-12 sm-padding-top-three xs-padding-top-six">
									<div class="column-innner-wrapper">
										<?php if ( $page->glanceBottomImage() != '' ) : ?>
											<?php $image =  thumb( $page->image( $page->glanceBottomImage() ), array( 'width' => 800, 'height' => 600, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
											<img alt="" class="xs-img-full" height="800" width="800" src="<?php echo $image->url(); ?>">

										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-6 col-xs-mobile-fullwidth col-sm-12 padding-six-top sm-padding-top-three xs-padding-top-six">
									<div class="column-innner-wrapper gray-text">
										<?php echo $page->glancetBottomContent()->kirbytext(); ?>

									</div>
								</div>

							<?php else : ?>

								<div class="col-md-6 col-xs-mobile-fullwidth col-sm-12 xs-padding-top-six sm-padding-top-six">
									<div class="column-innner-wrapper gray-text">
										<?php echo $page->glancetBottomContent()->kirbytext(); ?>
									</div>
								</div>
								<div class="col-md-6 col-xs-mobile-fullwidth col-sm-12 xs-padding-top-six sm-padding-top-six ipad-padding-top">
									<div class="column-innner-wrapper">
										<?php if ( $page->glanceBottomImage() != '' ) : ?>
											<?php $image =  thumb( $page->image( $page->glanceBottomImage() ), array( 'width' => 800, 'height' => 600, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
											
											<img alt="" class="xs-img-full" height="800" width="800" src="<?php echo $image->url(); ?>">
										<?php endif; ?>

									</div>
								</div>
							<?php endif; ?>

						</div>													
					</div>
				</section>
				