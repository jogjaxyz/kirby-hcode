				
				<!-- SECTION COUNTER -->
				<section class="section-counter wow fadeIn">
					<div class="container">
						<div class="row">
						<?php foreach ( $page->counterItems()->toStructure() as $item ) : ?>
							
							<div class="col-md-3 col-xs-mobile-fullwidth col-sm-6 text-center sm-margin-ten-bottom xs-margin-ten-bottom">
								<div class="column-innner-wrapper">
									<div class="counter-section">
										<span class="counter-number black-text" data-to="<?php echo $item->number(); ?>" id="counter_1"><?php echo $item->number(); ?></span>
										<span class="counter-title black-text"><?php echo $item->title(); ?></span>
									</div>
								</div>
							</div>
						<?php endforeach; ?>

						</div>
					</div>
				</section>
				