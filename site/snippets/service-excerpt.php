			<?php if ( $page->slug() == 'fun-dive' OR $page->slug() == 'snorkeling' ) : ?>
				
				<!-- SECTION ExCERPTS -->
				<section class="section-service-excerpt no-padding-top">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<h2 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->excerptTitle(); ?></h2>
									<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper content-left">
									<?php echo $page->excerptContent()->kirbytext(); ?>
								</div>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 animated fadeInUp">
								<div class="column-innner-wrapper content-right">
									<div class="embedded-map-wrapper">
										<?php echo $page->excerptMap()->kirbytext(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
			<?php endif; ?>
