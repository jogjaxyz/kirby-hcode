
	<!-- NAV MENUS -->
	<div class="col-md-8 no-padding-right text-right pull-right">
		<div class="navbar-collapse collapse">
			<ul class="mega-menu-ul nav navbar-nav navbar-right panel-group" id="menu-main-nav">
			 <?php foreach( $pages->visible() as $item ) : ?>
				<?php if ( $item->hasChildren() && $item->slug() == 'service' ) {
					$li_classes 	= 'dropdown panel simple-dropdown dropdown-toggle collapsed';
					$a_classes 		= 'class="dropdown-toggle collapsed"';
					$a_attributes 	= 'data-default-url="#collapse-'.$item->uid().'" data-hover="dropdown" data-redirect-url="'.$item->url().'" data-toggle="collapse" href="#collapse-'.$item->uid().'"';
				} else {
					$li_classes 	= '';
					$a_classes 		= '';
					$a_attributes 	= 'href="'.$item->url().'"';
				} ?>

				<li class="menu-item <?php echo $li_classes; ?> <?php echo r( $item->isOpen(), ' current-menu-item' ); ?>" id="menu-item-<?php echo $item->uid(); ?>">
					<a <?php echo $a_classes; ?> <?php echo $a_attributes; ?>><?php echo $item->title(); ?></a>
					<?php if ( $item->hasChildren() && $item->slug() == 'service' ) : ?>

					<a class="dropdown-toggle collapsed megamenu-right-icon" data-hover="dropdown" data-toggle="collapse" href="#collapse-<?php echo $item->uid(); ?>"><i class="fa fa-angle-down megamenu-mobile-icon"></i></a>
					<?php endif; ?>
					<?php if ( $item->hasChildren() && $item->slug() == 'service' ) : ?>
						
						<ul class="dropdown-menu mega-menu panel-collapse collapse" id="collapse-<?php echo $item->uid(); ?>">
						<?php $submenus = $item->children()->visible(); ?>
						<?php foreach ( $submenus as $sub ) : ?>
							
							<li class="menu-item" id="menu-item-23">
								<a href="<?php echo $sub->url(); ?>"><?php echo $sub->title(); ?></a>
							</li>
						<?php endforeach; ?>

						</ul>
					<?php endif; ?>

				</li>
			<?php endforeach; ?>
			
			</ul>
		</div>
	</div>