<form action="<?php echo $site->find( 'search' )->url(); ?>" class="mfp-hide search-form-result" id="search-header">
	<div class="search-form position-relative">
		<button class="fa fa-search close-search search-button black-text" type="submit"></button> 
		<input autocomplete="off" class="search-input" name="q" placeholder="<?php echo l::get( 'Enter your keywords...' ); ?>" type="text">
	</div>
</form>