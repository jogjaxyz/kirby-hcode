				
				<?php if ( $page->slug() == 'speedboat' ) : ?>
				
				<!-- SECTION SERVICE GALLERY -->
				<section class="section-service-gallery">
					<div class="container">
						<div class="row">
							<div class="col-xs-mobile-fullwidth">
								<div class="column-innner-wrapper">
									<div class="lightbox-gallery">
										<?php foreach ( $page->galleryItems()->toStructure() as $item ) : ?>
											<div class="col-md-4 col-sm-12 col-xs-12 no-padding sm-padding-two xs-padding-two">
												<?php $image =  thumb( $page->image( $item->image() ), array( 'width' => 395, 'height' => 300, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
												<a class="lightboxgalleryitem" data-group="default" href="<?php echo $item->image()->toFile()->url(); ?>">
													<img alt="" class="project-img-gallery no-padding" height="300" width="395" src="<?php echo $image->url(); ?>" >
												</a>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<?php endif; ?>
