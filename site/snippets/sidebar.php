<div class="col-md-3 col-sm-4 col-xs-12 col-md-offset-1 xs-margin-top-seven sidebar pull-right">
	<?php snippet( 'widget-search' ); ?>
	<?php snippet( 'widget-categories' ); ?>
	<?php snippet( 'widget-latest-posts' ); ?>
	<?php snippet( 'widget-tag-cloud' ); ?>
	<?php snippet( 'widget-archives' ); ?>
</div>