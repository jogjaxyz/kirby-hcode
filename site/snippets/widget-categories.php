<div class="widget widget_categories" id="categories-14">
	<h5 class="widget-title font-alt"><?php echo l::get( 'Categories' ); ?></h5>
	<div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
		<?php $categories = $pages->find( 'blog' )->children()->visible()->pluck( 'categories', ',', true ); ?>
		<ul class="categories">
			<?php if ( ! empty( $categories ) ) : ?>
				<?php foreach( $categories as $category ) : ?>
					<li class="cat-item widget-category-list light-gray-text">
						<a href="<?php echo url( 'blog/category:' . $category ); ?>"><?php echo html( ucfirst( $category ) ); ?></a>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
</div>