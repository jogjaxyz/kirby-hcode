				<?php 
					$get_heading_content 	= $site->headingContent();
				 ?>				
				<!-- SECTION SUBHEADING -->
				<section class="section-subheading" data-bg-color="#000000">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xs-mobile-fullwidth col-sm-8 text-center center-col margin-eight-top sm-margin-eight-top xs-margin-twelve-top">
								<div class="column-innner-wrapper">
									<div class="about-year text-uppercase">
										<h2 class="white-text"><?php echo $page->title(); ?></h2>
									</div>
									<p class="title-small text-uppercase letter-spacing-1 white-text font-weight-100">
										<?php echo $get_heading_content; ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				