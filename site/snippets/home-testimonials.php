			
			<!-- SECTION TESTIMONIALS -->
			<section class="section-testimonials" data-bg-color="#000000">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-mobile-fullwidth text-center margin-two-bottom">
							<div class="column-innner-wrapper">
								<h2 class="section-title white-text no-padding-bottom"><?php echo l::get( 'Testimonials' ); ?></h2>
							</div>
						</div>
						<div class="col-md-6 col-xs-mobile-fullwidth col-sm-10 text-center center-col">
							<div class="column-innner-wrapper">
								<div class="testimonial-slider position-relative no-transition">
									<div class="owl-pagination-bottom position-relative round-pagination light-pagination white-cursor" id="hcode-testimonial">
										<?php foreach ( $page->testimonialsItems()->toStructure() as $item ) : ?>
											<div class="col-md-12 col-sm-12 col-xs-12 testimonial-style2 center-col text-center margin-three no-margin-top">
												<p><?php echo html::decode( $item->content()->kirbytext() ); ?></p>
												<h4 class="name light-gray-text2" data-color="#737373">
													<?php echo html::decode( $item->name()->kirbytext() ); ?>
												</h4>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			