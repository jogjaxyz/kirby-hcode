<?php if ( $pagination->hasPages() ) : ?>

	<div class="pagination pull-left">
		<?php if ( $pagination->hasPrevPage() ) : ?>
			<a class="next" href="<?php echo $pagination->prevPageURL() ?>">
				<img alt="Next" src="<?php echo url( 'assets/images/arrow-pre-small.png' ); ?>" width="20" height="13">
			</a>
		<?php endif; ?>
		<?php foreach( $pagination->range(5) as $paging ) : ?>
			<a href="<?php echo $pagination->pageURL( $paging ); ?>"><?php echo $paging; ?></a>
		<?php endforeach; ?>
		<?php if ( $pagination->hasNextPage() ) : ?>
			<a class="prev page-numbers" href="<?php echo $pagination->nextPageURL(); ?>">
				<img alt="Previous" src="<?php echo url( 'assets/images/arrow-next-small.png' ); ?>" width="20" height="13">
			</a>
		<?php endif; ?>
	</div>
	
<?php endif; ?>
