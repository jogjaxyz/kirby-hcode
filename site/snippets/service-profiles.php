				
				<!-- SECTION PROFILES -->
				<section class="section-profiles">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<h2 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->serviceTitle(); ?></h2>
									<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper content-left">
									<?php echo $page->serviceLeftContent()->kirbytext(); ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper content-right">
									<?php echo $page->serviceRightContent()->kirbytext(); ?>
								</div>
							</div>
						</div>
					</div>
				</section>
				