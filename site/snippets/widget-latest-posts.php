<div class="widget widget_hcode_recent_widget" id="hcode_recent_widget-14">
	<h5 class="widget-title font-alt"><?php echo l::get( 'Latest posts' ); ?></h5>
	<div class="thin-separator-line bg-dark-gray no-margin-lr"></div><!-- end heading -->
	<div class="widget-body">
		<?php $latest = $pages->find( 'blog' )->children()->visible(); ?>
		<ul class="widget-posts">
			<?php if ( ! empty( $latest ) ) : ?>
				<?php foreach( $latest as $post ) : ?>
					<li class="clearfix">
						<a href="<?php echo $post->url(); ?>">
							<?php $featured_image =  thumb( $post->image(), array( 'width' => 760, 'height' => 570, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
							<img alt="760x570-ph alt" class="attachment-full size-full wp-post-image" height="570" src="<?php echo $featured_image->url(); ?>" title="" width="760">
						</a>
						<div class="widget-posts-details">
							<a href="<?php echo $post->url(); ?>"><?php echo $post->title(); ?></a><?php echo $post->author(); ?> - <?php echo $post->date( 'd F Y' );  ?>
						</div>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</div>