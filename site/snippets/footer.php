	
	<!-- FOOTER -->
	<footer class="bg-light-gray2">
		<div class="bg-white footer-top">
			<div class="container">
				<div class="row margin-four">
					<div class="col-md-4 col-sm-4 text-center">
						<i class="icon-phone small-icon black-text"></i>
						<h6 class="black-text margin-two no-margin-bottom"><?php echo html::decode( $site->phoneNumber()->kirbytext() ); ?></h6>
					</div>
					<div class="col-md-4 col-sm-4 text-center">
						<i class="icon-map-pin small-icon black-text"></i>
						<h6 class="black-text margin-two no-margin-bottom"><?php echo html::decode( $site->location()->kirbytext() ); ?></h6>
					</div>
					<div class="col-md-4 col-sm-4 text-center">
						<i class="icon-envelope small-icon black-text"></i>
						<h6 class="margin-two no-margin-bottom"><a class="black-text" href="mailto:<?php echo html::decode( $site->emailAddress()->kirbytext() ); ?>"><?php echo html::decode( $site->emailAddress()->kirbytext() ); ?></a></h6>
					</div>
				</div>
			</div>
		</div>

		<div class="container footer-middle">
			<div class="row">
				<div class="col-md-4 col-sm-3 sm-display-none">
						<div class="widget_text">
						<h5 class="sidebar-title">RED WHALE DIVING CENTER</h5>
						<p class="footer-text">
									Kampung Ujung Lingkungan I, 02/02, Labuan Bajo<br>Nusa Tenggara Timur, Flores, Indonesia<br>
									+62 (81) 3375-91335 (Eng + Rus)<br>
									info@redwhaledc.com
									</p>
									</div>
					</div>
			<?php foreach ( $site->footerWidgetsitems()->toStructure() as $widgets ) : ?>
				<?php if ( 'info' == $widgets->widgetType() ) : ?>
					
					<div class="col-md-4 col-sm-3 sm-display-none">
						<div class="widget_text">
							<h5 class="sidebar-title"><?php echo $widgets->widgetTitle(); ?></h5><!-- end headline -->
							<div class="textwidget">
								<p class="footer-text">
									<?php echo html::decode( $widgets->widgetContent() ); ?>
								</p>
							</div>
						</div>
					</div>
				<?php else : ?>
					
					<div class="col-md-2 col-sm-3 col-xs-3 footer-links">
						<div class="widget_hcode_custom_menu_widget" id="hcode_custom_menu_widget-8">
							<h5 class="sidebar-title"><?php echo $widgets->widgetTitle(); ?></h5><!-- end headline -->
							<?php echo $widgets->widgetContent()->kirbytext(); ?>
							
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

				<div class="col-md-2 col-sm-3 col-xs-12">
					<div class="widget-redwhale-sponsor-image-idget text-center xs-text-center" id="redwhale-sponsor-image_widget-8">
						<a href="" class="diver-network"><img src="<?php echo kirby()->urls()->assets(); ?>/images/diver-network.png" alt="Diver Network"></a>
						<a href="" class="trip-advisor padding-left-two"><img src="<?php echo kirby()->urls()->assets(); ?>/images/trip-advisor.png" alt="Trip Advisor"></a>
					</div>
				</div>
			</div>
			<div class="wide-separator-line bg-mid-gray no-margin-lr margin-three no-margin-bottom"></div>
		</div>

		<div class="container-fluid bg-dark-gray footer-bottom">
			<div class="container">
				<div class="row margin-three">
					<div class="col-md-6 col-sm-6 col-xs-12 copyright text-left letter-spacing-1 xs-text-center xs-margin-bottom-one light-gray-text2">
						<?php echo html::decode( $site->copyright()->kirbytext() ); ?>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 footer-social text-right xs-text-center">
						<a class="white-text-link" href="<?php echo html::decode( $site->facebook()->kirbytext() ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
						<a class="white-text-link" href="<?php echo html::decode( $site->twitter()->kirbytext() ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
						<a class="white-text-link" href="<?php echo html::decode( $site->gplus()->kirbytext() ); ?>" target="_blank"><i class="fa fa-google-plus"></i></a> 
						<a class="white-text-link" href="<?php echo html::decode( $site->pinterest()->kirbytext() ); ?>" target="_blank"><i class="fa fa-pinterest"></i></a> 
						<a class="white-text-link" href="<?php echo html::decode( $site->instagram()->kirbytext() ); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
					</div>
				</div>
			</div>
		</div>

		 <a class="scrollToTop" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
	</footer>

	<?php echo js('assets/js/plugins.min.js'); ?>

	<?php echo js('assets/js/main.js'); ?>

</body>
</html>