<div class="widget widget_search" id="search-10">
	<form action="<?php echo $site->find( 'search' )->url(); ?>" class="main-search" id="search" method="get" name="search">
		<button class="fa fa-search close-search search-button black-text" type="submit"></button> <input class="sidebar-search" name="q" placeholder="<?php echo l::get( 'Enter your keywords...' ); ?>" type="text" value="">
	</form>
</div>