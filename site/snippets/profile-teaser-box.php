				
				<!-- SECTION TEASER BOX -->
				<section class="fix-background" data-bg-image="<?php echo $page->teaserBgImage()->toFile()->url(); ?>">
					<div class="selection-overlay" data-bg-color="#252525" data-opacity="0.7"></div>
					<div class="container">
						<div class="row">
							<div class="col-xs-mobile-fullwidth">
								<div class="column-innner-wrapper">
									<div class="slider-typography container position-relative">
										<div class="slider-text-middle-main">
											<div class="slider-text-middle text-center slider-text-middle1 center-col wow fadeIn">
												<span class="fashion-subtitle text-uppercase font-weight-700 border-color-white text-center white-text">
													<?php echo $page->teaserTitle(); ?>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				