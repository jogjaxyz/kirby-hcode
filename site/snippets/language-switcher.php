<div class="top-lang">
	<select class="language-switcher" onchange="document.location.href=this.options[this.selectedIndex].value;">
		<?php foreach( $site->languages() as $language ) : ?>
			<option<?php e( $site->language() == $language, ' selected' ); ?> value="<?php echo $language->url(); ?>">
				<?php echo html( $language->name() ); ?>
			</option>
		<?php endforeach ?>
	</select>
</div>
