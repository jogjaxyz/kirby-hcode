				<?php if ( $page->slug() == 'fun-dive' OR $page->slug() == 'snorkeling' ) : ?>

				<!-- SECTION SCHEDULE -->
				<section class="section-service-schedule wow fadeIn">
					<div class="container">
						<div class="row">
							<table class="table table-striped">
								<tbody>
									<?php foreach ( $page->scheduleItems()->toStructure() as $item ) : ?>

									<tr>
										<td class="col-md-3">
											<strong><?php echo $item->title(); ?></strong>
										</td>
										<td class="col-md-9">
											<?php echo $item->content(); ?>

										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>

						</div>
					</div>
				</section>

				<?php endif; ?>
				