				
				<!-- SECTION EXCERPTS -->
				<section class="section-excerpts" data-bg-color="#f6f6f6">
					<div class="container">
						<div class="row">
						<?php foreach ( $page->excerptItems()->toStructure() as $item ) : ?>
							
							<div class="col-md-4 col-xs-mobile-fullwidth col-sm-4 text-center xs-margin-bottom-ten">
								<div class="column-innner-wrapper">
									<?php if ( s::get( 'device_class' ) == 'mobile' ) : 
										$key_class = '-mobile';
									else : 
										$key_class = '';
									endif; ?>
									<div class="key-person<?php echo $key_class; ?>">
										<div class="key-person-img">
											<?php $image =  thumb( $page->image( $item->image() ), array( 'width' => 800, 'height' => 800, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
											<img alt="" height="1000" src="<?php echo $image->url(); ?>" width="800">
										</div>
										<div class="key-person-details bg-gray no-border no-padding-bottom">
											<h5 class="text-med"><?php echo $item->title(); ?></h5>
											<div class="separator-line bg-black"></div>
											<p><?php echo $item->content(); ?></p>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
						</div>
					</div>
				</section>
