			
			<!-- SECTION INTRO -->
			<section class="section-intro">
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-xs-mobile-fullwidth">
							<div class="column-innner-wrapper">
								<h2 class="text-large display-block"><?php echo html::decode( $page->introTitle()->kirbytext() ); ?></h2>
								<h3 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text">
									<?php echo html::decode( $page->introSubtitle()->kirbytext() ); ?>
								</h3>
								<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
								<?php echo $page->introContent()->kirbytext(); ?>
								<a class="highlight-button-dark btn btn-small no-margin-bottom inner-link sm-margin-bottom-ten" href="<?php echo $page->introLink(); ?>" target="_self"><?php echo l::get( 'Read more' ); ?></a>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-6 col-xs-mobile-fullwidth col-sm-12 text-center xs-margin-ten-bottom">
							<div class="column-innner-wrapper">
								<?php if ( $page->introImage() != '' ) : ?>
									<?php $image =  thumb( $page->image( $page->introImage() ), array( 'width' => 800, 'height' => 800, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
									<img alt="" class="xs-img-full" height="800" width="800" src="<?php echo $image->url(); ?>">
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>