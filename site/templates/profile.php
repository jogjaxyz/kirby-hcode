<?php snippet( 'header' ); ?>
	
	<!-- PARENT SECTION -->
	<section class="parent-section no-padding">
		<div class="container-fluid">
			<div class="row">

				<?php snippet( 'subheading' ); ?>
				<?php snippet( 'profile-profiles' ); ?>
				<?php snippet( 'profile-glance' ); ?>
				<?php snippet( 'profile-excerpt' ); ?>
				<?php snippet( 'profile-counter' ); ?>
				<?php snippet( 'profile-teaser-box' ); ?>

			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>