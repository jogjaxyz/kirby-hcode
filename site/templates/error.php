<?php snippet( 'header' ); ?>
	
	<!-- PARENT SECTION -->
	<section class="no-padding cover-background full-screen wow fadeIn" data-bg-image="<?php echo kirby()->urls()->assets(); ?>/images/page-not-found.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-sm-12 col-xs-12 text-center center-col full-screen">
					<div class="col-text-center">
						<div class="col-text-middle-main">
							<div class="col-text-middle">
								<p class="not-found-title white-text"></p>
								<p class="title-small xs-title-small xs-display-none text-uppercase letter-spacing-2 white-text"><?php echo l::get( 'The page you were looking<br>
								for could not be found.' ); ?></p>
								<div class="not-found-search-box">
									<a class="btn-small-white btn btn-medium no-margin-right" href="<?php echo $site->homePage()->url() ?>"><?php echo l::get( 'Go to home page' ); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>