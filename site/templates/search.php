<?php snippet( 'header' ) ?>
	
<?php snippet( 'subheading' ); ?>
	
<?php if ( $query ) : ?>
	
	<section class="page-title page-title-small bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
					<h1 class="black-text"><?php echo l:;get( 'Search For' ); ?> "<?php echo $query; ?>"</h1>
				</div>
			</div>
		</div>
	</section>
	
	<!-- PARENT SECTION -->
	<section class="parent-section">
		<div class="container col2-layout">
			<div class="row">
				
				<?php if ( $results != '' ) : ?>
					
					<div class="col-md-8 col-sm-8 col-xs-12">
						<section class=" xs-margin-ten-bottom no-padding">
							<div class="container-fluid">
								<div class="row">
									<div class="col-xs-mobile-fullwidth">
										<div class="column-innner-wrapper">
											
											<?php foreach ( $results as $result ) : ?>
												<?php snippet( 'post', array( 'item' => $result ) ); ?>
											<?php endforeach; ?>
											<?php snippet( 'pagination' ); ?>

										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
					
					<?php else : ?>
						<?php snippet( 'not-found' ); ?>
				<?php endif; ?>		

				<?php snippet( 'sidebar' ); ?>
			</div>
		</div>
	</section>

<?php endif; ?>

<?php snippet('footer') ?>