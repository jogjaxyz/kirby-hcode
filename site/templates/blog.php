<?php snippet( 'header' ) ?>
	
	<section class="content-top-margin page-title page-title-small bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
					<h1 class="black-text"><?php echo $page->title()->html() ?></h1>
				</div>
			</div>
		</div>
	</section>
	
	<section class="parent-section">
		<div class="container col2-layout">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12">
					<section class=" xs-margin-ten-bottom no-padding">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-mobile-fullwidth">
									<div class="column-innner-wrapper">
										
										<?php if ( $articles->count() ) : ?>
											<?php foreach( $articles as $article ) : ?>
												
												<?php snippet( 'post', array( 'item' => $article ) ); ?>
											
											<?php endforeach ?>
											<?php else: ?>
												<p><?php echo l::get( 'This blog does not contain any articles yet.' ); ?></p>
										<?php endif; ?>

										<?php snippet( 'pagination' ); ?>

									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<?php snippet( 'sidebar' ); ?>
			</div>
		</div>
	</section>


<?php snippet('footer') ?>