<?php snippet('header') ?>

	<section class="parent-section no-padding section-hero">
		<div class="container-fluid">
			<div class="row">
				<?php snippet( 'home-hero' ); ?> 
				<?php snippet( 'home-intro' ); ?>
				<?php snippet( 'home-teaser-box' ); ?>
				<?php snippet( 'home-features' ); ?>
				<?php snippet( 'home-testimonials' ); ?>
				<?php snippet( 'home-blog' ); ?>
			</div>
		</div>
	</section>

<?php snippet('footer') ?>