<?php snippet( 'header' ); ?>
	
	<!-- PARENT SECTION -->
	<section class="parent-section no-padding">
		<div class="container-fluid">
			<div class="row">

				<?php snippet( 'subheading' ); ?>
				
				<!-- SECTION TOURS -->
				<section class="section-tours">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<!-- DIVING PROGRAMMES -->
									<div class="diving-programmes-wrapper">
										<div class="heading-style-five  margin-five sm-margin-seven-bottom xs-margin-seven-bottom">
											<h5 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo l::get( 'Diving Programmes' ); ?></h5>
											<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
										</div>

										<div class="panel-group accordion-style3" id="accordion-one">
											<?php $dp = 1; ?>
											<?php foreach ( $page->divingProgrammesItems()->toStructure() as $item ) : ?>
												<div class="panel panel-default">
													<div class="panel-heading">
														<a class="collapsed" data-parent="#accordion-one" data-toggle="collapse" href="#accordian-panel-<?php echo $dp; ?>">
														<h4 class="panel-title"><?php echo $item->title(); ?><span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
													</div>
													<div class="panel-collapse collapse" id="accordian-panel-<?php echo $dp; ?>">
														<div class="panel-body">
															<p class="no-margin"><?php echo $item->content()->kirbytext(); ?></p>
														</div>
													</div>
												</div>
											<?php $dp++; endforeach; ?>

										</div>
									</div>
									
									<!-- SNORKELING PROGRAMMES -->
									<div class="snorkeling-programmes-wrapper">
										<div class="heading-style-five  margin-five sm-margin-seven-bottom xs-margin-seven-bottom">
											<h5 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo l::get( 'Snorkeling Programmes' ); ?></h5>
											<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
										</div>

										<div class="panel-group accordion-style3" id="accordion-two">
											<?php $sp = 1; ?>
											<?php foreach ( $page->snorkelingProgrammesItems()->toStructure() as $item ) : ?>
												<div class="panel panel-default">
													<div class="panel-heading">
														<a class="collapsed" data-parent="#accordion-two" data-toggle="collapse" href="#accordian-panel-sp<?php echo $sp; ?>">
														<h4 class="panel-title"><?php echo $item->title(); ?><span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
													</div>
													<div class="panel-collapse collapse" id="accordian-panel-sp<?php echo $sp; ?>">
														<div class="panel-body">
															<p class="no-margin"><?php echo $item->content()->kirbytext(); ?></p>
														</div>
													</div>
												</div>
											<?php $sp++; endforeach; ?>

										</div>
									</div>

									<!-- FLORES LAND TOURS PROGRAMMES -->
									<div class="floreslandtours-programmes-wrapper">
										<div class="heading-style-five  margin-five sm-margin-seven-bottom xs-margin-seven-bottom">
											<h5 class="title-large text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo l::get( 'Flores Land Tours Programmes' ); ?></h5>
											<div class="separator-line-thick bg-fast-pink no-margin-lr"></div>
										</div>

										<div class="panel-group accordion-style3" id="accordion-three">
											<?php $fltp = 1; ?>
											<?php foreach ( $page->floresLandToursItems()->toStructure() as $item ) : ?>
												<div class="panel panel-default">
													<div class="panel-heading">
														<a class="collapsed" data-parent="#accordion-three" data-toggle="collapse" href="#accordian-panel-flt<?php echo $fltp; ?>">
														<h4 class="panel-title"><?php echo $item->title(); ?><span class="pull-right"><i class="fa fa-angle-down"></i></span></h4></a>
													</div>
													<div class="panel-collapse collapse" id="accordian-panel-flt<?php echo $fltp; ?>">
														<div class="panel-body">
															<p class="no-margin"><?php echo $item->content()->kirbytext(); ?></p>
														</div>
													</div>
												</div>
											<?php $fltp++; endforeach; ?>

										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<div class="widget widget-information bg-gray">
										<h5 class="title-small text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->sidebarWidgetTitleOne(); ?></h5>
										<div class="widget-image padding-five-top padding-six-bottom">
											<?php if ( $page->sidebarWidgetImageOne() != '' ) : ?>
												<?php $image =  thumb( $page->image( $page->sidebarWidgetImageOne() ), array( 'width' => 324, 'height' => 243, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
												<img alt="" class="xs-img-full" height="243" width="324" src="<?php echo $image->url(); ?>">
											<?php endif; ?>
										</div>
										<div class="widget-content">
											<?php echo $page->sidebarWidgetContentOne(); ?>
										</div>
									</div>
									<div class="widget widget-information bg-gray">
										<h5 class="title-small text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->sidebarWidgetTitleTwo(); ?></h5>
										<div class="widget-image padding-five-top padding-six-bottom">
											<?php if ( $page->sidebarWidgetImageTwo() != '' ) : ?>
												<?php $image =  thumb( $page->image( $page->sidebarWidgetImageTwo() ), array( 'width' => 324, 'height' => 243, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
												<img alt="" class="xs-img-full" height="243" width="324" src="<?php echo $image->url(); ?>">
											<?php endif; ?>
										</div>
										<div class="widget-content">
											<?php echo $page->sidebarWidgetContentTwo(); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>