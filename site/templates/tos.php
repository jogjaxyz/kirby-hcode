<?php snippet( 'header' ); ?>
	
	<section class="content-top-margin page-title page-title-small bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
					<h1 class="black-text"><?php echo $page->title()->html() ?></h1>
				</div>
			</div>
		</div>
	</section>
	
	<!-- PARENT SECTION -->
	<section class="parent-section no-padding">
		<div class="container-fluid">
			<div class="row">
				
				<!-- SECTION TOS -->
				<section class="section-tos">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<?php echo $page->text()->kirbytext(); ?>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 animated fadeInUp">
								<div class="column-innner-wrapper">
									<div class="widget widget-information bg-gray">
										<h5 class="title-small text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->sidebarWidgetTitleOne(); ?></h5>
										<div class="widget-image padding-five-top padding-six-bottom">
											<?php if ( $page->sidebarWidgetImageOne() != '' ) : ?>
												<?php $image =  thumb( $page->image( $page->sidebarWidgetImageOne() ), array( 'width' => 324, 'height' => 243, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
												<img alt="" class="xs-img-full" height="243" width="324" src="<?php echo $image->url(); ?>">
											<?php endif; ?>
										</div>
										<div class="widget-content">
											<?php echo $page->sidebarWidgetContentOne(); ?>
										</div>
									</div>
									<div class="widget widget-information bg-gray">
										<h5 class="title-small text-uppercase letter-spacing-1 font-weight-600 black-text"><?php echo $page->sidebarWidgetTitleTwo(); ?></h5>
										<div class="widget-image padding-five-top padding-six-bottom">
											<?php if ( $page->sidebarWidgetImageTwo() != '' ) : ?>
												<?php $image =  thumb( $page->image( $page->sidebarWidgetImageTwo() ), array( 'width' => 324, 'height' => 243, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?>
												<img alt="" class="xs-img-full" height="243" width="324" src="<?php echo $image->url(); ?>">
											<?php endif; ?>
										</div>
										<div class="widget-content">
											<?php echo $page->sidebarWidgetContentTwo(); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>