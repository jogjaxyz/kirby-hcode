<?php snippet( 'header' ); ?>
	
	<!-- PARENT SECTION -->
	<section class="parent-section no-padding">
		<div class="container-fluid">
			<div class="row">

				<?php snippet( 'subheading' ); ?>

				<!-- SECTION TESTIMONIALS -->
				<section class="section-testimonials">
					<div class="container">
						<div class="row">
							
							<?php foreach ( $page->testimonialsItems()->toStructure() as $item ) : ?>

								<div class="col-md-6 col-sm-6 col-xs-12 testimonial-style2 text-center margin-three no-margin-top">
									<p><?php echo html::decode( $item->content()->kirbytext() ); ?></p>
									<h4 class="name light-gray-text2" data-color="#737373">
										<?php echo html::decode( $item->name()->kirbytext() ); ?> - <?php echo $item->title(); ?>
									</h4>
									<i class="fa fa-quote-left small-icon display-block margin-five no-margin-bottom" data-color="#e6af2a"></i>
								</div>
							<?php endforeach; ?>
							
						</div>
					</div>
				</section>

			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>