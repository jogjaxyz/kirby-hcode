<?php snippet( 'header' ); ?>
	
	<!-- PARENT SECTION -->
	<section class="parent-section no-padding">
		<div class="container-fluid">
			<div class="row">

				<?php snippet( 'subheading' ); ?>
				<?php snippet( 'service-profiles' ); ?>
				<?php snippet( 'service-glance' ); ?>
				<?php snippet( 'service-schedule' ); ?>
				<?php snippet( 'service-gallery' ); ?>
				<?php snippet( 'service-excerpt' ); ?>

			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>