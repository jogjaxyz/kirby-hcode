<?php snippet( 'header' ); ?>

	<section class="content-top-margin page-title page-title-small bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
					<h1 class="black-text"><?php echo l::get( 'Blog' ); ?></h1>
				</div>
			</div>
		</div>
	</section>

	<section class="no-padding-bottom post type-post">
		<div class="container col2-layout">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12">
					<section class="no-padding next-prev-post-wrapper" itemscope="" itemtype="http://schema.org/BlogPosting">
						<div class="container">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
									<h1 class="blog-details-headline text-black" itemprop="name"><?php echo $page->title(); ?></h1>
									<div class="blog-date no-padding-top" itemprop="datePublished">
										Posted by <?php echo $page->author(); ?> | <?php echo $page->date( 'd F Y' );  ?> | <?php getPostCategories( $page ); ?>
									</div>
									<div class="margin-tb-30px">
										<div class="blog-image bg-transparent">
											<?php $image =  thumb( $page->image( $page->featuredImage() ), array( 'width' => 760, 'height' => 570, 'crop' => true, 'quality' => 80, 'blur' => false  ) ); ?><img alt="760x570-ph alt" class="attachment-full size-full wp-post-image" height="570" width="760" src="<?php echo $image->url(); ?>" title="">
										</div>
									</div>
									<div class="blog-details-text" itemprop="articleBody">
										<section class=" no-padding">
											<div class="col-xs-mobile-fullwidth">
												<div class="vc-column-innner-wrapper">
													<?php echo $page->text()->kirbytext(); ?>
												</div>
											</div>
										</section>
									</div>
									<div class="text-center padding-four-top padding-four-bottom col-md-12 col-sm-12 col-xs-12 no-padding-lr">
										<div class="share-post" data-url="<?php echo rawurlencode( $page->url() ); ?>" data-text="<?php echo html::decode( $page->text()->kirbytext() ); ?>" data-title="<?php echo rawurlencode( $page->title() ); ?>"></div>
									</div>
									<div class="comments-area border-top padding-ten-top" id="comments">
										<?php snippet( 'disqus', array( 
												'disqus_shortname' => $site->disqusShortName(), 
												'disqus_developer' => true 
										) ); ?>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				
				<?php snippet( 'sidebar' ); ?>
				<?php snippet( 'related-post' ); ?>
				
			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>