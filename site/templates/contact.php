<?php snippet( 'header' ); ?>

	<section class="content-top-margin page-title page-title-small border-bottom-light border-top-light bg-white">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
					<h1 class="black-text"><?php echo $page->title()->html() ?></h1>
				</div>
			</div>
		</div>
	</section>

	<section class="parent-section no-padding">
		<div class="container-fluid">
			<div class="row">
				<section class=" no-padding">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-mobile-fullwidth no-padding">
								<div class="column-innner-wrapper">
									<div class="contact-map map" id="canvas1">
										<?php echo $page->mapcode()->kirbytext(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				<section id="contact-us">
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-xs-mobile-fullwidth col-sm-6">
								<div class="column-innner-wrapper">
									<?php echo $page->thecontent()->kirbytext(); ?>
								</div>
							</div>
							
							<div class="col-md-offset-2 col-md-6 col-xs-mobile-fullwidth col-sm-6">
								<div class="column-innner-wrapper">
									<form method="post">
										
										<?php if ( $alert ) : ?>
											
											<?php if ( isset( $alert[0] ) && $alert[0] == 'Your Message Sent' ) : ?>
												<?php $alert_class = 'alert-success'; ?>
											<?php else : ?>
												<?php $alert_class = 'alert-warning'; ?>
											<?php endif; ?>

											<div class="alert <?php echo $alert_class; ?>">
												<ul>
													<?php foreach( $alert as $message ) : ?>
														<li><?php echo html( $message ); ?></li>
													<?php endforeach; ?>
												</ul>
											</div>
										<?php endif; ?>

										<div class="no-margin">
											<input type="text" name="name" size="40" aria-required="true" aria-invalid="false" placeholder="YOUR NAME">
											<input type="email" name="email" size="40" aria-required="true" aria-invalid="false" placeholder="YOUR EMAIL">
											<textarea name="text" cols="40" rows="2" aria-invalid="false" placeholder="YOUR MESSAGE"></textarea>
										</div>

										<input type="submit" name="submit" value="Send Message" class="highlight-button-dark btn btn-small button xs-margin-bottom-five">
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>