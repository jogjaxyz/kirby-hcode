<?php snippet( 'header' ) ?>
	
	<?php snippet( 'subheading' ); ?>
	
	<section class="parent-section">
		<div class="container col2-layout">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12">
					<section class=" xs-margin-ten-bottom no-padding">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-mobile-fullwidth">
									<div class="column-innner-wrapper">
										<h1><?php echo $page->title()->html() ?></h1>
										<?php echo $page->text()->kirbytext() ?>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<?php snippet( 'sidebar' ); ?>
			</div>
		</div>
	</section>


<?php snippet('footer') ?>