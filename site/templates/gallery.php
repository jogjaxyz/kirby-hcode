<?php snippet( 'header' ); ?>
	
	<section class="content-top-margin page-title page-title-small bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7 col-sm-12 animated fadeInUp">
					<h1 class="black-text"><?php echo $page->title()->html() ?></h1>
				</div>
			</div>
		</div>
	</section>
	
	<section class="parent-section padding-three">
		<div class="container">
			<div class="row">
				<div class="col-xs-mobile-fullwidth">
					<div class="column-innner-wrapper">
						<div class="lightbox-gallery">
							<?php foreach ( $galleries as $item ) { ?>
								<?php 
									$image = $item->images()->sortBy( 'sort', 'asc' )->first(); 
									$thumb = $image->crop( 365, 365 );
								?>
								<div class="col-md-4 col-sm-6">
									<a class="lightboxgalleryitem" data-group="default" href="<?php echo $image->url(); ?>"><img alt="<?php echo $item->title(); ?>" class="project-img-gallery" height="365" width="365" src="<?php echo $thumb->url(); ?>" ></a>
								</div>
							<?php } ?>
							<?php snippet( 'pagination' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php snippet( 'footer' ); ?>