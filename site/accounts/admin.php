<?php if(!defined('KIRBY')) exit ?>

username: admin
password: >
  $2a$10$UnCr4ZwUWIMdbFmj2FkqQ.QlYcn361KiAAO5yYpUU8rPvz1suRYMm
email: alispx@gmail.com
language: en
role: admin
history:
  - home
  - service/speedboat
  - blog/tado-village-a-balance-with-nature
  - blog/loh-liang-national-park
  - blog/all-about-kukusan-island
