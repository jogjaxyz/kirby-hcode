Title: Home

----

Intro: 

----

Text: 

----

Herotitle: Красный Кит Центр подводного плавания

----

Herosubtitle: Enjoy in-depth itinerary, fastest speedboat, pro instructor, excellent equipment and amazing food with professional dive center around Komodo Island.

----

Herobgimage: homepagebg.jpg

----

Introtitle: CAPACITY TO SEE BEAUTY

----

Introsubtitle: OUR MISSION IS TO SATISFY

----

Introcontent: 

In 1991, Komodo National Park was declared as UNESCO World Heritage Site. The park includes the three larger islands; Komodo, Padar, and Rinca. Komodo National Park is the only home of the famous giant lizard, Komodo dragon. Their unusually large size draws interest of many scientist to study about the theory of evolution.

Komodo National Park offers a dramatic landscapes between the dry savannah vegetation, green rugged hillsides, and white sandy beaches in contrast with the clear blue sea water. Utterly breathtaking panorama.

Komodo National Park’s fauna is quite a contrast between the terrestrial and marine fauna. The terrestrial fauna is not as rich as its marine life. Komodo National Park host a great number of species under its sea, such as shark, manta ray, turtle, whale, dolphin, eagle rays, pygmy seahorse, false pipefish, clown frogfish, nudibranchs, blue-ringed octopus, sponges, tunicates and many more.

----

Introlink: http://testing.com

----

Introimage: intros.jpg

----

Abouttitle: We Love Diving

----

Aboutbgimage: blog_1.jpg

----

Teasertitle: WE LOVE DIVING

----

Teaserbgimage: homepagebg.jpg

----

Featuredserviceimage: intro.jpg

----

Featuredservicesitems: 

- 
  title: FUN DIVE
  content: 'Lorem Ipsum is simply dummy text of the printing & typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry’s standard dummy text.'
  link: http://tester.com
  image: intro.jpg
- 
  title: SNORKLING
  content: 'Lorem Ipsum is simply dummy text of the printing & typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry’s standard dummy text.'
  link: http://testers.com
  image: intro.jpg
- 
  title: SPEEDBOAT
  content: 'Lorem Ipsum is simply dummy text of the printing & typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry’s standard dummy text.'
  link: http://testersss.com
  image: intro.jpg
- 
  title: TOURS
  content: 'Lorem Ipsum is simply dummy text of the printing & typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry’s standard dummy text.'
  link: http://tours.com
  image: intro.jpg

----

Testimonialsitems: 

- 
  name: 'Johndoekoen - Google Inc'
  content: 'Focused, hard work is the real key to success. Keep your eyes on the goal, and just keep taking the next step towards completing it. If you aren&#8217;t sure which way to do something, do it both ways and see which works better.'
- 
  name: 'Mark Jungkir Balik - Fesbook Inc'
  content: 'Focused, hard work is the real key to success. Keep your eyes on the goal, and just keep taking the next step towards completing it. If you aren&#8217;t sure which way to do something, do it both ways and see which works better.'

----

Description: Lorem ipsum dolor sit amet consectetur